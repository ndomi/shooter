﻿//-----------------------------------------------------------------------
// <copyright file="Karakter.cs" company="comp">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Prog3_shooter
{
    /// <summary>
    /// Karakter class
    /// </summary>
    public abstract class Karakter : Bindable
    {
        /// <summary>
        /// Karakter x pozíciója
        /// </summary>
        private int pozx;

        /// <summary>
        /// Karakter y pozíciója
        /// </summary>
        private int pozy;

        /// <summary>
        /// Karakter mérete, magasság és szélesség
        /// </summary>
        private int meret;

        /// <summary>
        /// Karakter kitöltése
        /// </summary>
        private Brush kitoltes;

        /// <summary>
        /// Karakterek ütközéséhez használt Rect
        /// </summary>
        private Rect utk;

        /// <summary>
        /// Initializes a new instance of the Karakter class.
        /// Karakter konstruktora
        /// </summary>
        /// <param name="x">X eltolás</param>
        /// <param name="y">Y eltolás</param>
        /// <param name="meret">Karakter mérete</param>
        public Karakter(int x, int y, int meret)
        {
            this.Poz_x = x;
            this.Poz_y = y;
            this.Meret = meret;
            this.Utk = new Rect((double)x, (double)y, (double)meret, (double)meret);
        }

        /// <summary>
        /// Gets or sets pozx
        /// </summary>
        public int Poz_x
        {
            get
            {
                return this.pozx;
            }

            set
            {
                this.pozx = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets pozy
        /// </summary>
        public int Poz_y
        {
            get
            {
                return this.pozy;
            }

            set
            {
                this.pozy = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets meret
        /// </summary>
        public int Meret
        {
            get
            {
                return this.meret;
            }

            set
            {
                this.meret = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets kitoltes
        /// </summary>
        public Brush Kitoltes
        {
            get
            {
                return this.kitoltes;
            }

            set
            {
                this.kitoltes = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets utk
        /// </summary>
        public Rect Utk
        {
            get
            {
                return this.utk;
            }

            set
            {
                this.utk = value;
                this.OPC();
            }
        }
    }
}
