﻿//-----------------------------------------------------------------------
// <copyright file="Tolteny.cs" company="comp">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3_shooter
{
    /// <summary>
    /// Tolteny class
    /// </summary>
    internal class Tolteny : Karakter
    {
        /// <summary>
        /// Töltény dx irányvektora
        /// </summary>
        private int dx;

        /// <summary>
        /// Töltény dy irányvektora
        /// </summary>
        private int dy;

        /// <summary>
        /// Initializes a new instance of the Tolteny class.
        /// Tolteny konstruktor
        /// </summary>
        /// <param name="x">Töltény x pozíciója</param>
        /// <param name="y">Töltény y pozíciója</param>
        /// <param name="meret">Töltény mérete</param>
        public Tolteny(int x, int y, int meret) : base(x, y, meret)
        {
            this.Kitoltes = new ImageBrush(new BitmapImage(new Uri("tear.png", UriKind.RelativeOrAbsolute)));
        }

        /// <summary>
        /// Gets or sets dx
        /// töltény x irányvektora
        /// </summary>
        public int Dx
        {
            get
            {
                return this.dx;
            }

            set
            {
                this.dx = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets dy
        /// töltény y irányvektora
        /// </summary>
        public int Dy
        {
            get
            {
                return this.dy;
            }

            set
            {
                this.dy = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Töltény mozgása, poziciókhoz hozzáadja az irányvektorok értékeit, beállítja a Rect objektumot
        /// </summary>
        public void Mozgat()
        {
            this.Poz_x += this.Dx;
            this.Poz_y += this.Dy;
            this.Utk = new Rect(Poz_x, Poz_y, Meret, Meret);
        }
    }
}
