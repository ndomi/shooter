﻿//-----------------------------------------------------------------------
// <copyright file="Jatek_ablak.xaml.cs" company="comp">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prog3_shooter
{
    /// <summary>
    /// Interaction logic for Jatek_ablak.xaml
    /// </summary>
    public partial class Jatek_ablak : Window
    {
        /// <summary>
        /// Szoba létrehozása
        /// </summary>
        private Szoba sZB;

        /// <summary>
        /// Lenyomottgombok gyüjtemény létrehozása
        /// </summary>
        private ObservableCollection<Key> lenyomottgombok;

        /// <summary>
        /// Tick létrehozáa
        /// </summary>
        private System.Windows.Threading.DispatcherTimer mozgasLoves;

        /// <summary>
        /// Tick létrehozáa
        /// </summary>
        private System.Windows.Threading.DispatcherTimer ellensegletrehozas;

        /// <summary>
        /// Tick létrehozáa
        /// </summary>
        private System.Windows.Threading.DispatcherTimer ellensegmozgas;

        /// <summary>
        /// Tick létrehozáa
        /// </summary>
        private System.Windows.Threading.DispatcherTimer ellensegsebez;

        /// <summary>
        /// Initializes a new instance of the Jatek_ablak class.
        /// </summary>
        public Jatek_ablak()
        {
            this.InitializeComponent();

            this.sZB = new Szoba(this.jatekter);
            this.lenyomottgombok = new ObservableCollection<Key>();
            this.DataContext = this.sZB;
            this.KeyDown += this.MainWindow_KeyDown;
            this.KeyUp += this.MainWindow_KeyUp;

            ImageBrush ib = new ImageBrush();
            ib.ImageSource = new BitmapImage(new Uri("map.png", UriKind.RelativeOrAbsolute));
            this.jatekter.Background = ib;

            this.mozgasLoves = new System.Windows.Threading.DispatcherTimer();
            this.mozgasLoves.Interval = TimeSpan.FromMilliseconds(10);
            this.mozgasLoves.Tick += this.Mozgas_Loves_Tick;
            this.mozgasLoves.Start();

            this.ellensegletrehozas = new System.Windows.Threading.DispatcherTimer();
            this.ellensegletrehozas.Interval = TimeSpan.FromMilliseconds(0);
            this.ellensegletrehozas.Tick += this.Ellenseg_letrehozas_Tick;
            this.ellensegletrehozas.Start();
            this.ellensegletrehozas.Interval = TimeSpan.FromMilliseconds(10000);

            this.ellensegmozgas = new System.Windows.Threading.DispatcherTimer();
            this.ellensegmozgas.Interval = TimeSpan.FromMilliseconds(5);
            this.ellensegmozgas.Tick += this.Ellenseg_mozgas_Tick;
            this.ellensegmozgas.Start();

            this.ellensegsebez = new System.Windows.Threading.DispatcherTimer();
            this.ellensegsebez.Interval = TimeSpan.FromMilliseconds(5000);
            this.ellensegsebez.Tick += this.Ellenseg_sebez_Tick;
        }

        /// <summary>
        /// Az ellenség sebzésének késleltetésére használt tick metódus
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e e</param>
        private void Ellenseg_sebez_Tick(object sender, EventArgs e)
        {
            this.sZB.Sebezhet = true;
        }

        /// <summary>
        /// Tick metódus, játékos mozgás, lövés mozgás, töltény törlés, ellenség sebzése(töltények, ellenségek törlése)
        /// győzelem vizsgálata(boss legyőzve, lement az összes hullám)
        /// </summary>
        /// <param name="sender">senser s</param>
        /// <param name="e">e e</param>
        private void Mozgas_Loves_Tick(object sender, EventArgs e)
        {
            this.sZB.Mozgas((int)jatekter.ActualWidth, (int)jatekter.ActualHeight, this.lenyomottgombok);
            this.sZB.Loves_mozgat();
            this.sZB.ToltenyTorles((int)jatekter.ActualWidth, (int)jatekter.ActualHeight);
            this.sZB.Sebzes(this.jatekter);

            if (this.sZB.Hullam == this.sZB.JatekHossz && this.sZB.Ellensegek.Count == 0)
            {
                this.sZB.Hullam--;
                this.mozgasLoves.Stop();
                this.ellensegletrehozas.Stop();
                this.ellensegmozgas.Stop();
                this.ellensegsebez.Stop();
                MessageBoxResult bezarVujrakezd = MessageBox.Show("Újrakezd-Yes, Főmenö-No", "Nyertél!!!", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (bezarVujrakezd == MessageBoxResult.Yes)
                {
                    this.DialogResult = true;
                    Jatek_ablak jatek = new Jatek_ablak();
                    jatek.ShowDialog();
                }
                else
                {
                    this.DialogResult = true;
                }
            }
        }

        /// <summary>
        /// Tick metódus, hullámok kezelése (hullám számoltatás, powerupok), ellenség létrehozása hullámokban, utolsó hullámnál boss létrehozása,
        /// powerUp beadása messageboxxal
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e e</param>
        private void Ellenseg_letrehozas_Tick(object sender, EventArgs e)
        {
            if (this.sZB.Hullam < this.sZB.JatekHossz)
            {
                this.sZB.Hullam++;
            }
                
                if (this.sZB.Hullam < this.sZB.JatekHossz)
                {
                    for (int i = 0; i < 10; i++)
                    {
                    this.sZB.Ellenseg_letrehoz(this.jatekter);
                    }
                }
                else if (this.sZB.Hullam == this.sZB.JatekHossz && this.sZB.Ellensegek.Count == 0)
                {
                    this.sZB.Ellenseg_letrehoz(this.jatekter);
                }

            if (this.sZB.Hullam % this.sZB.PowerUpHullamSzam == 0 && this.sZB.Hullam != this.sZB.JatekHossz + 1)
            {
                this.lenyomottgombok = new ObservableCollection<Key>();
                this.mozgasLoves.Stop();
                this.ellensegletrehozas.Stop();
                this.ellensegmozgas.Stop();
                this.ellensegsebez.Stop();
                MessageBoxResult sebzesVeletero = MessageBox.Show("Sebzés +1 -Yes, Életerő +1 -No", "Power up", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (sebzesVeletero == MessageBoxResult.Yes)
                {
                    this.sZB.Jatekos.Sebzes++;
                }
                else
                {
                    this.sZB.Jatekos.Eletero++;
                }

                this.mozgasLoves.Start();
                this.ellensegletrehozas.Start();
                this.ellensegmozgas.Start();
                this.ellensegsebez.Start();
            }
         }

        /// <summary>
        /// Tick metódus, ellenség mozgása, játékos sebzése(játékos életerő vesztése, annak késleltetése), game over vizsgálat 
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e e</param>
        private void Ellenseg_mozgas_Tick(object sender, EventArgs e)
        {
            this.sZB.Ellenseg_mozgas((int)jatekter.ActualWidth, (int)jatekter.ActualHeight);
            this.sZB.JatekosSebzes();
            if (this.sZB.Jatekos.Eletero <= 0)
            {
                this.mozgasLoves.Stop();
                this.ellensegletrehozas.Stop();
                this.ellensegmozgas.Stop();
                this.ellensegsebez.Stop();
                MessageBoxResult bezarVujrakezd = MessageBox.Show("Újrakezd-Yes, Főmenö-No", "Game over", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (bezarVujrakezd == MessageBoxResult.Yes)
                {
                    this.DialogResult = true;
                    Jatek_ablak jatek = new Jatek_ablak();
                    jatek.ShowDialog();
                }
                else
                {
                    this.DialogResult = true;
                }
            }

            if (!this.sZB.Sebezhet)
            {
                this.ellensegsebez.Start();
            }
        }

        /// <summary>
        /// Lenyomott gombok eltárolása listában, mozgáshoz, lövéshez
        /// Kilépés vizsgálata, esc gomb segítségével
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e e</param>
        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
         {
             if (this.lenyomottgombok.Contains(e.Key))
             {
                return;
             }
                 
            this.lenyomottgombok.Add(e.Key);
             e.Handled = true;
             if (e.Key == Key.W || e.Key == Key.S || e.Key == Key.A || e.Key == Key.D)
             {
                 this.sZB.Loves(this.jatekter, this.lenyomottgombok);
             }

            if (e.Key == Key.Escape)
            {
                this.mozgasLoves.Stop();
                this.ellensegletrehozas.Stop();
                this.ellensegmozgas.Stop();
                this.ellensegsebez.Stop();
                MessageBoxResult bezarVujrakezd = MessageBox.Show("Kilépés-Yes, Mégse-No", "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (bezarVujrakezd == MessageBoxResult.Yes)
                {
                    this.DialogResult = true;
                }
                else
                {
                    this.mozgasLoves.Start();
                    this.ellensegletrehozas.Start();
                    this.ellensegmozgas.Start();
                    this.ellensegsebez.Start();
                }
            }
         }

        /// <summary>
        /// Lenyomott gombok gyüjteményből kiveszi a felengedett gombokat
        /// </summary>
        /// <param name="sender"> sender s</param>
        /// <param name="e">e e</param>
         private void MainWindow_KeyUp(object sender, KeyEventArgs e)
         {
             this.lenyomottgombok.Remove(e.Key);
             e.Handled = true;
         }
    }
}
