﻿//-----------------------------------------------------------------------
// <copyright file="Jatekos.cs" company="comp">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3_shooter
{
    /// <summary>
    /// Jatekos class
    /// </summary>
    internal class Jatekos : Karakter
    {
        /// <summary>
        /// Játékos életereje
        /// </summary>
        private int eletero;

        /// <summary>
        /// Játékos sebzése
        /// </summary>
        private int sebzes;

        /// <summary>
        /// Initializes a new instance of the Jatekos class.
        /// Játékos konstruktor
        /// </summary>
        /// <param name="x">Játékos x pozíciója</param>
        /// <param name="y">Játékos y pozíciója</param>
        /// <param name="meret">Játékos mérete</param>
        public Jatekos(int x, int y, int meret) 
            : base(x, y, meret)
        {
            this.Kitoltes = new ImageBrush(new BitmapImage(new Uri("isaac.png", UriKind.RelativeOrAbsolute)));
            this.Eletero = 3;
            this.Sebzes = 1;
        }

        /// <summary>
        /// Gets or sets eletero
        /// </summary>
        public int Eletero
        {
            get
            {
                return this.eletero;
            }

            set
            {
                this.eletero = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets sebzes
        /// </summary>
        public int Sebzes
        {
            get
            {
                return this.sebzes;
            }

            set
            {
                this.sebzes = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Játékos mozgatása
        /// </summary>
        /// <param name="x">Játékos pozíciójának növelésére használt x paraméter</param>
        /// <param name="y">Játékos pozíciójának növelésére használt y paraméter</param>
        public void Mozgat(int x, int y)
        {
            this.Poz_x += x;
            this.Poz_y += y;
            this.Utk = new Rect(Poz_x, Poz_y, Meret, Meret);
        }
    }
}
