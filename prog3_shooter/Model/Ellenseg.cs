﻿//-----------------------------------------------------------------------
// <copyright file="Ellenseg.cs" company="comp">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3_shooter
{
    /// <summary>
    /// Ellenseg class
    /// </summary>
    internal class Ellenseg : Karakter
    {
        /// <summary>
        /// Ellenség életereje
        /// </summary>
        private int eletero;

        /// <summary>
        /// Ellenség x irányú irányvektora
        /// </summary>
        private int dx;
        
        /// <summary>
        /// Ellenség y irányú irányvektora
        /// </summary>
        private int dy;

        /// <summary>
        /// Ellenség azonosítója 
        /// (ellenségek egymással való ütközésénél van használva)
        /// </summary>
        private int id;

        /// <summary>
        /// Initializes a new instance of the Ellenseg class.
        /// </summary>
        /// <param name="x">Ellenség x pozíciója</param>
        /// <param name="y">Ellenség y pozíciója</param>
        /// <param name="meret">Ellenség mérete</param>
        /// <param name="eletero">Ellenség életereje</param>
        /// <param name="kep">Ellenség megjelenítése</param>
        public Ellenseg(int x, int y, int meret, int eletero, string kep)
            : base(x, y, meret)
        {
            this.Kitoltes = new ImageBrush(new BitmapImage(new Uri(kep, UriKind.RelativeOrAbsolute)));
            this.Eletero = eletero;
        }

        /// <summary>
        /// Gets or sets eletero
        /// </summary>
        public int Eletero
        {
            get
            {
                return this.eletero;
            }

            set
            {
                this.eletero = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets dx
        /// irányvektor
        /// </summary>
        public int Dx
        {
            get
            {
                return this.dx;
            }

            set
            {
                this.dx = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets dy
        /// irányvektor
        /// </summary>
        public int Dy
        {
            get
            {
                return this.dy;
            }

            set
            {
                this.dy = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets id
        /// azonositó
        /// </summary>
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Ellenség mozgása, poziciókhoz hozzáadja az irányvektorok értékeit, beállítja a Rect objektumot
        /// </summary>
        public void Mozgas()
        {
            this.Poz_x += this.Dx;
            this.Poz_y += this.Dy;
            this.Utk = new Rect(Poz_x, Poz_y, Meret, Meret);
        }
    }
}
