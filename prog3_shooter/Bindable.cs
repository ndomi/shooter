﻿//-----------------------------------------------------------------------
// <copyright file="Bindable.cs" company="comp">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Prog3_shooter
{
    /// <summary>
    /// Bindable class
    /// </summary>
    public abstract class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// propertychaned adattag
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Initializes a new instance of the Bindable class.
        /// </summary>
        /// <param name="s">s string</param>
        protected void OPC([CallerMemberName] string s = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(s));
            }
        }
    }
}
