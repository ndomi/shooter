﻿//-----------------------------------------------------------------------
// <copyright file="Szoba.cs" company="comp">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Shapes;

namespace Prog3_shooter
{
    /// <summary>
    /// Szoba class
    /// ViewModel
    /// </summary>
    internal class Szoba : Bindable
    {
        /// <summary>
        /// Random r
        /// </summary>
        private static Random r = new Random();

        /// <summary>
        /// Megjelenítésre használt canvas
        /// </summary>
        private Canvas map;

        /// <summary>
        /// Karakter sérüléséhez használom, ha igaz, akkor sebezhető
        /// </summary>
        private bool sebezhet = true;

        /// <summary>
        /// Jatekos osztály példánya
        /// </summary>
        private Jatekos jatekos;

        /// <summary>
        /// Töltények gyüjteménye
        /// </summary>
        private ObservableCollection<Tolteny> loszerek;

        /// <summary>
        /// Ellenségek gyüjteménye
        /// </summary>
        private ObservableCollection<Ellenseg> ellensegek;

        /// <summary>
        /// Ellenségek megjelenítésének gyüjteménye (Ellipse,törlésre tárolva)
        /// </summary>
        private ObservableCollection<Ellipse> ellensegektorlesre;

        /// <summary>
        /// Lőszerek megjelenítésének gyűjteménye (Ellipse, törlésre tárolva)
        /// </summary>
        private ObservableCollection<Ellipse> loszerektorlesre;

        /// <summary>
        /// Ellenség hullámok száma
        /// </summary>
        private int hullam;

        /// <summary>
        /// Össz hullám száma
        /// </summary>
        private int jatekHossz;

        /// <summary>
        /// Ennyi hullámonként lehet powerup-t választani
        /// </summary>
        private int powerUpHullamSzam;

        /// <summary>
        /// Initializes a new instance of the Szoba class.
        /// Szoba konstruktora
        /// </summary>
        /// <param name="jatekter">Canvas átadása</param>
        public Szoba(Canvas jatekter)
        {
            this.jatekos = new Jatekos(350, 240, 75);
            this.Loszerek = new ObservableCollection<Tolteny>();
            this.Ellensegek = new ObservableCollection<Ellenseg>();
            this.Loszerek_torlesre = new ObservableCollection<Ellipse>();
            this.Ellensegek_torlesre = new ObservableCollection<Ellipse>();
            this.Hullam = 0;
            this.JatekHossz = 10;
            this.PowerUpHullamSzam = 3;
            this.map = jatekter;
        }

        /// <summary>
        /// Gets or sets jatekos
        /// </summary>
        public Jatekos Jatekos
        {
            get
            {
                return this.jatekos;
            }

            set
            {
                this.jatekos = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets loszerek
        /// </summary>
        public ObservableCollection<Tolteny> Loszerek
        {
            get
            {
                return this.loszerek;
            }

            set
            {
                this.loszerek = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the sebezhet is enabled
        /// </summary>
        public bool Sebezhet
        {
            get
            {
                return this.sebezhet;
            }

            set
            {
                this.sebezhet = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets ellensegek
        /// </summary>
        public ObservableCollection<Ellenseg> Ellensegek
        {
            get
            {
                return this.ellensegek;
            }

            set
            {
                this.ellensegek = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets ellensegektorlesre
        /// </summary>
        public ObservableCollection<Ellipse> Ellensegek_torlesre
        {
            get
            {
                return this.ellensegektorlesre;
            }

            set
            {
                this.ellensegektorlesre = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets loszerektorlesre
        /// </summary>
        public ObservableCollection<Ellipse> Loszerek_torlesre
        {
            get
            {
                return this.loszerektorlesre;
            }

            set
            {
                this.loszerektorlesre = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets hullam
        /// </summary>
        public int Hullam
        {
            get
            {
                return this.hullam;
            }

            set
            {
                this.hullam = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets jatekHossz
        /// </summary>
        public int JatekHossz
        {
            get
            {
                return this.jatekHossz;
            }

            set
            {
                this.jatekHossz = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Gets or sets powerUpHullamSzam
        /// </summary>
        public int PowerUpHullamSzam
        {
            get
            {
                return this.powerUpHullamSzam;
            }

            set
            {
                this.powerUpHullamSzam = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Játékos mozgása, ha a lenyomottgombok lista tartalmazza a megadott gombot,
        /// akkor abba az irány fog haladni a karakter
        /// </summary>
        /// <param name="abl_szel">canvas szélessége</param>
        /// <param name="abl_mag">canvas magassága</param>
        /// <param name="l">lenyomott gombok gyüjteménye</param>
        public void Mozgas(int abl_szel, int abl_mag, ObservableCollection<Key> l)
        {
            if (l.Contains(Key.Up) && this.Jatekos.Poz_y > 0)
            {
                this.Jatekos.Mozgat(0, -5);
            }

            if (l.Contains(Key.Down) && this.Jatekos.Poz_y + this.Jatekos.Meret < abl_mag)
            {
                this.Jatekos.Mozgat(0, 5);
            }

            if (l.Contains(Key.Left) && this.Jatekos.Poz_x > 0)
            {
                this.Jatekos.Mozgat(-5, 0);
            }

            if (l.Contains(Key.Right) && this.Jatekos.Poz_x + this.Jatekos.Meret < abl_szel)
            {
                this.Jatekos.Mozgat(5, 0);
            }
        }

        /// <summary>
        /// Lövedék létrehozása, hozzá tartozó ellipse létrehozása,
        /// ezek gyüjteményekben való eltárolása
        /// ellipse-hez töltény adatkötése
        /// ellipse canvas-hoz hozzáadása
        /// </summary>
        /// <param name="map">megjelenítésre használt canvas</param>
        /// <param name="l">Lenyomott gombok, melyik irányba menjen a töltény, töltény irányvektorának beállítása</param>
        public void Loves(Canvas map, ObservableCollection<Key> l)
        {
            Tolteny uj = new Tolteny(this.jatekos.Poz_x, this.jatekos.Poz_y, 15);
            Ellipse el = new Ellipse();
            if (l.Contains(Key.W))
            {
                uj.Poz_x += 30;
                uj.Dx = 0;
                uj.Dy = -5;
            }
            else if (l.Contains(Key.S))
            {
                uj.Poz_x += 30;
                uj.Poz_y += 60;
                uj.Dx = 0;
                uj.Dy = 5;
            }
            else if (l.Contains(Key.A))
            {
                uj.Poz_x += 0;
                uj.Poz_y += 30;
                uj.Dx = -5;
                uj.Dy = 0;
            }
            else if (l.Contains(Key.D))
            {
                uj.Poz_x += 60;
                uj.Poz_y += 30;
                uj.Dx = 5;
                uj.Dy = 0;
            }

            el.DataContext = uj;
            el.SetBinding(Ellipse.FillProperty, new Binding("Kitoltes"));
            el.SetBinding(Ellipse.WidthProperty, new Binding("Meret"));
            el.SetBinding(Ellipse.HeightProperty, new Binding("Meret"));
            el.SetBinding(Canvas.TopProperty, new Binding("Poz_y"));
            el.SetBinding(Canvas.LeftProperty, new Binding("Poz_x"));
            this.Loszerek.Add(uj);
            this.loszerektorlesre.Add(el);
            map.Children.Add(el);
        }

        /// <summary>
        /// Minden lőszer mozgat metódusának hívása
        /// </summary>
        public void Loves_mozgat()
        {
            for (int i = 0; i < this.Loszerek.Count; i++)
            {
                this.Loszerek[i].Mozgat();
            }
        }

        /// <summary>
        /// Ellenség létrehozása, hozzá tartozó ellipse létrehozása,
        /// ezek gyüjteményekben való eltárolása
        /// ellipse-hez ellenség adatkötése
        /// ellipse canvas-hoz hozzáadása
        /// </summary>
        /// <param name="map">Megjelenítésre használt canvas</param>
        public void Ellenseg_letrehoz(Canvas map)
        {
            int randomEledesKordinata = r.Next(3);
            int randomEllensegek = r.Next(3);
            string kitoltes = string.Empty;
            int ellensegEletero = 0;
            int ellensegMeret = 0;
            int x = 0;
            int y = 0;
            switch (randomEledesKordinata)
            {
                case 0:
                    x = r.Next(0, (int)map.ActualWidth - 30);
                    y = r.Next(0, 30);
                    break;
                case 1:
                    x = r.Next(0, 30);
                    y = r.Next(0, (int)map.ActualHeight - 30);
                    break;
                case 2:
                    x = r.Next((int)map.ActualWidth - 60, (int)map.ActualWidth - 30);
                    y = r.Next(0, (int)map.ActualHeight - 30);
                    break;
            }

            switch (randomEllensegek)
            {
                case 0:
                    kitoltes = "boomfly.png";
                    ellensegEletero = 1;
                    ellensegMeret = 25;
                    break;
                case 1:
                    kitoltes = "redboomfly.png";
                    ellensegEletero = 2;
                    ellensegMeret = 30;
                    break;
                case 2:
                    kitoltes = "drownedboomfly.png";
                    ellensegEletero = 3;
                    ellensegMeret = 35;
                    break;
            }

            if (this.Hullam == this.JatekHossz)
            {
                kitoltes = "bluebaby.png";
                ellensegEletero = 100;
                ellensegMeret = 75;
            }

            if (this.Hullam % 3 == 0)
            {
                for (int i = 0; i < this.Hullam / 3; i++)
                {
                    ellensegEletero++;
                }
            }

            Ellenseg uj = new Ellenseg(x, y, ellensegMeret, ellensegEletero, kitoltes);
            Ellipse el = new Ellipse();
            el.DataContext = uj;
            el.SetBinding(Ellipse.FillProperty, new Binding("Kitoltes"));
            el.SetBinding(Ellipse.WidthProperty, new Binding("Meret"));
            el.SetBinding(Ellipse.HeightProperty, new Binding("Meret"));
            el.SetBinding(Canvas.TopProperty, new Binding("Poz_y"));
            el.SetBinding(Canvas.LeftProperty, new Binding("Poz_x"));
            this.Ellensegek.Add(uj);
            uj.ID = this.Ellensegek.Count();
            map.Children.Add(el);
            this.Ellensegek_torlesre.Add(el);
            this.Ellenseg_vektor2(uj);
        }

        /// <summary>
        /// 8 irányból random irány-t ad az ellenségnek,
        /// akkor használom, ha két ellenség ütközik, így nem kerülnek egymás allá véglegesen.
        /// </summary>
        /// <param name="ellenseg">Adott ellenség</param>
        public void Ellenseg_vektor(Ellenseg ellenseg)
        {
            int dis = 5;
                int irany = r.Next(8);
                switch (irany)
                {
                    case 0:
                        ellenseg.Dx = dis;
                        ellenseg.Dy = 0;
                        break;
                    case 1:
                        ellenseg.Dx = -dis;
                        ellenseg.Dy = 0;
                        break;
                    case 2:
                        ellenseg.Dx = 0;
                        ellenseg.Dy = -dis;
                        break;
                    case 3:
                        ellenseg.Dx = 0;
                        ellenseg.Dy = dis;
                        break;
                    case 4:
                        ellenseg.Dx = dis;
                        ellenseg.Dy = -dis;
                        break;
                    case 5:
                        ellenseg.Dx = dis;
                        ellenseg.Dy = dis;
                        break;
                    case 6:
                        ellenseg.Dx = -dis;
                        ellenseg.Dy = dis;
                        break;
                    case 7:
                        ellenseg.Dx = -dis;
                        ellenseg.Dy = -dis;
                        break;
                }
        }

        /// <summary>
        /// Végigmegyek az ellenségek listán, meghívom rá a mozgás metódusokat,
        /// </summary>
        /// <param name="abl_szel">canvas szélessége</param>
        /// <param name="abl_mag">canvas magassága</param>
        public void Ellenseg_mozgas(int abl_szel, int abl_mag)
        {
            for (int i = 0; i < this.Ellensegek.Count; i++)
            {
               this.Ellensegek[i].Mozgas();
               this.Ellenseg_vektor2(this.Ellensegek[i]);
               this.Ellenseg_mozgas_ellenorzes2(this.Ellensegek[i], abl_szel, abl_mag);
            }
        }

        /// <summary>
        /// Ellenörzöm hogy nem megy-e ki a szobából az ellenség, ha igen, meghívom rá a játékos követésére használt metódust
        /// </summary>
        /// <param name="ellenseg">Adott ellenség</param>
        /// <param name="abl_szel">canvas szélessége</param>
        /// <param name="abl_mag">canvas magassága</param>
        public void Ellenseg_mozgas_ellenorzes(Ellenseg ellenseg, int abl_szel, int abl_mag)
        {
            if (ellenseg.Poz_x == 0)
            {
                do
                {
                    this.Ellenseg_vektor2(ellenseg);
                }
                while (ellenseg.Dx < 0);
            }

            if (ellenseg.Poz_x >= abl_szel - ellenseg.Meret)
            {
                do
                {
                    this.Ellenseg_vektor2(ellenseg);
                }
                while (ellenseg.Dx > 0);
            }

            if (ellenseg.Poz_y == 0)
            {
                do
                {
                    this.Ellenseg_vektor2(ellenseg);
                }
                while (ellenseg.Dy < 0);
            }

            if (ellenseg.Poz_y  >= abl_mag - ellenseg.Meret)
            {
                do
                {
                    this.Ellenseg_vektor2(ellenseg);
                }
                while (ellenseg.Dy > 0);
            }
        }

        /// <summary>
        /// Ellenőrzi az adott ellenség koordinátáit, ha nem egyenlő akkor megfelelőre állítja azokat, hogy közelítsenek a játékoshoz,
        /// ha egyelőek vele, 0-ra állítja, tehát megállnak
        /// </summary>
        /// <param name="ellenseg">Adott ellenség</param>
        public void Ellenseg_vektor2(Ellenseg ellenseg)
        {
                if (ellenseg.Poz_x < (this.Jatekos.Poz_x + (Jatekos.Meret / 2)))
                {
                    ellenseg.Dx = 1;
                }
                else if (ellenseg.Poz_x > (this.Jatekos.Poz_x + (Jatekos.Meret / 2)))
                {
                    ellenseg.Dx = -1;
                }
                else
                {
                ellenseg.Dx = 0;
                }

                if (ellenseg.Utk.IntersectsWith(Jatekos.Utk))
                {
                    ellenseg.Dx = 0;
                }

                if (ellenseg.Poz_y < (this.Jatekos.Poz_y + (Jatekos.Meret / 2)))
                {
                    ellenseg.Dy = 1;
                }
                else if (ellenseg.Poz_y > (this.Jatekos.Poz_y + (Jatekos.Meret / 2)))
                {
                    ellenseg.Dy = -1;
                }
                else
                {
                 ellenseg.Dy = 0;
                }

                if (ellenseg.Utk.IntersectsWith(Jatekos.Utk))
                {
                    ellenseg.Dy = 0;
                }
        }

        /// <summary>
        /// Ellenörzi, hogy az ellenségek ütköznek-e egymással, ha igen, meghívja a random irányba indító metódust,
        /// majd a fallal ütközést ellenőrző metódust (amiben meghívodik a játékos követés metódus),
        /// ezzel azt elérve, hogy ne menjenek egymásba az ellenségek, és kövessék az játékost
        /// </summary>
        /// <param name="ellenseg">Adott ellenség</param>
        /// <param name="abl_szel">canvas szélessége</param>
        /// <param name="abl_mag">canvas magassága</param>
        public void Ellenseg_mozgas_ellenorzes2(Ellenseg ellenseg, int abl_szel = 0, int abl_mag = 0)
        {
            for (int j = 0; j < this.Ellensegek.Count; j++)
            {
                if (ellenseg.Utk.IntersectsWith(this.Ellensegek[j].Utk) && ellenseg.ID != this.Ellensegek[j].ID)
                {
                    this.Ellenseg_vektor(ellenseg);
                    this.Ellenseg_mozgas_ellenorzes(ellenseg, abl_szel, abl_mag);
                }
            }
        }

        /// <summary>
        /// Töltények törlése, ha elérik a szoba szélét
        /// </summary>
        /// <param name="abl_szel">canvas szélessége</param>
        /// <param name="abl_mag">canvas magassága</param>
        public void ToltenyTorles(int abl_szel = 0, int abl_mag = 0)
        {
            for (int i = 0; i < this.loszerek.Count; i++)
            {
                if (this.loszerek != null && this.loszerek.Count > i)
                {
                    if (this.loszerek[i].Poz_x == 0)
                    {
                        this.loszerek.Remove(this.loszerek[i]);
                        this.map.Children.Remove(this.loszerektorlesre[i]);
                        this.loszerektorlesre.Remove(this.loszerektorlesre[i]);
                    }
                    else if (this.loszerek[i].Poz_x >= abl_szel - this.loszerek[i].Meret)
                    {
                        this.loszerek.Remove(this.loszerek[i]);
                        this.map.Children.Remove(this.loszerektorlesre[i]);
                        this.loszerektorlesre.Remove(this.loszerektorlesre[i]);
                    }
                    else if (this.loszerek[i].Poz_y == 0)
                    {
                        this.loszerek.Remove(this.loszerek[i]);
                        this.map.Children.Remove(this.loszerektorlesre[i]);
                        this.loszerektorlesre.Remove(this.loszerektorlesre[i]);
                    }
                    else if (this.loszerek[i].Poz_y >= abl_mag - this.loszerek[i].Meret)
                    {
                        this.loszerek.Remove(this.loszerek[i]);
                        this.map.Children.Remove(this.loszerektorlesre[i]);
                        this.loszerektorlesre.Remove(this.loszerektorlesre[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Nézi, hogy a töltény eltalja-e, az ellenséget, ha igen, levon egyet az életerejéből,
        /// ha nullára, vagy nulla alá csökken, törli a töltények, ellipse gyüjteményből, és a canvas children-i közül
        /// </summary>
        /// <param name="map">canvas átadás</param>
        public void Sebzes(Canvas map)
        {
            for (int i = 0; i < this.Loszerek.Count; i++)
            {
                for (int j = 0; j < this.Ellensegek.Count; j++)
                {
                    if (this.Loszerek != null && this.Loszerek.Count > i)
                    {
                        if (this.Loszerek[i].Utk.IntersectsWith(this.Ellensegek[j].Utk))
                        {
                            this.Loszerek.Remove(this.Loszerek[i]);
                            this.map.Children.Remove(this.loszerektorlesre[i]);
                            this.loszerektorlesre.Remove(this.loszerektorlesre[i]);

                            this.Ellensegek[j].Eletero -= this.Jatekos.Sebzes;
                            if (this.Ellensegek[j].Eletero <= 0)
                            {
                                this.Ellensegek.Remove(this.Ellensegek[j]);
                                this.map.Children.Remove(this.Ellensegek_torlesre[j]);
                                this.Ellensegek_torlesre.Remove(this.ellensegektorlesre[j]);
                            }

                            this.map.DataContext = this;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Játékos életvesztése, ha találkozik egy ellenséggel levon egyet a játékos életerejéből, minden levonás után 5 másodpercig
        /// sebezhetetlen a játékos, ha egyenlő lesz nullával vége a játéknak
        /// </summary>
        public void JatekosSebzes()
        {
            if (this.Sebezhet)
            {
                for (int i = 0; i < this.Ellensegek.Count; i++)
                {
                    if (this.Ellensegek[i].Utk.IntersectsWith(this.Jatekos.Utk) && this.Sebezhet)
                    {
                        this.Jatekos.Eletero--;
                        this.Sebezhet = false;
                    }
                }
            }
        }
    }
}
