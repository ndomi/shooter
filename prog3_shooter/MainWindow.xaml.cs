﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="comp">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prog3_shooter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// MainWindow konstruktora
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            ImageBrush ib = new ImageBrush();
            ib.ImageSource = new BitmapImage(new Uri("menu.jpg", UriKind.RelativeOrAbsolute));
            this.menu.Background = ib;

            this.KeyDown += this.MainWindow_KeyDown;
        }

        /// <summary>
        /// Játék elindítása, gomb
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e e</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Jatek_ablak jatek = new Jatek_ablak();
            jatek.ShowDialog();
        }

        /// <summary>
        /// Leírás menü-be lépés
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e e</param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.help.Visibility = Visibility.Visible;
            this.menu1.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// KeyDown esemény, menübe visszalépésre, a leírás menüből
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e e</param>
        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            this.help.Visibility = Visibility.Hidden;
            this.menu1.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Menüből való kilépés, kilépés gomb
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e e</param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mr = MessageBox.Show("Biztos kilép a játékból?", "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (mr == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
    }
}
